#Owl Carousel Legacy

Repackaging by c33s of Bartosz Wojciechowski's "Owl Carousel 1.3.3" (MIT License)

Bought the [Mazel template][mazel] at [wrapbootstrap][wrap] which sadly has very old library versions bundled. partly with modified 
vendor files. 
to use the theme with [symfony][symfony] it was necessary to refactor it to work with [webpack-encore][encore], twig, 
[yarn][yarn] and to allow to use more current versions.

[this lib (version 1.x)][owl1] is currently unmaintained and also exists in a deprecated [version 2.x][owl2] which 
recommends to use [tiny slider][tiny]. 

Repackaged by c33s, sources from [NuGet-Community-Packages][owl1-src2] as the [official repo][owl1-src] is empty.

To use this old version you can add the following to your `packages.yaml` (i use yaml for configs and convert it the 
json on the fly):
```
devDependencies:
    '@c33s/owl-carousel-legacy': https://gitlab.com/c33s-group/owl-carousel-legacy.git#master
```  

[symfony]: https://symfony.com/
[wrap]: https://wrapbootstrap.com/
[mazel]: https://wrapbootstrap.com/theme/mazel-multipurpose-bootstrap-4-theme-WB0BG4711
[encore]: https://github.com/symfony/webpack-encore
[yarn]: https://yarnpkg.com/
[owl1]: http://www.landmarkmlp.com/js-plugin/owl.carousel/
[owl1-src]: https://github.com/OwlFonk/OwlCarousel
[owl1-src2]: https://github.com/Geta/NuGet-Community-Packages/tree/master/OwlCarousel/Content/Content/OwlCarousel
[owl2]: https://owlcarousel2.github.io/OwlCarousel2/
[tiny]: https://github.com/ganlanyuan/tiny-slider
